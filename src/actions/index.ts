import * as home from './home';
import * as demo from './demo';

const actions: any = {
  home,
  demo
};

export default actions;
