import React, { useEffect, useLayoutEffect } from 'react';
import { connect } from 'react-redux';
import { DEMO_GET_CONF } from '../actions/demo';

const store = ({ demo }: any) => ({ demo });
const event = (dispatch: any) => {
  return {
    getConfData() {
      dispatch({
        type: DEMO_GET_CONF.name
      });
    }
  };
};
function Demo(props: any) {
  const { getConfData, demo: { conf: { color = '#fff' } = {} } = {} } = props;

  useEffect(() => {
    console.log(19);
    getConfData();
    return () => {};
  }, [getConfData]);

  useLayoutEffect(() => {
    console.log(color);
    return () => {};
  }, [color]);

  return (
    <div>
      <h1>DEMO</h1>
      <div style={{ width: '100%', height: '100px', backgroundColor: color }}></div>
    </div>
  );
}

export default connect(store, event)(Demo);
