# @fekit/inview

This plugin can set page elements to play animations when they enter the visible area, so as to build a web page with an awesome experience.

## Catalog

- [Demos](#Demos)
- [Install](#Install)
- [Options](#Options)
- [Example](#Example)
- [Themes](#Themes)
- [Version](#Version)

## Demos

[https://mcui.fekit.cn/#route=plugins/js/inview](https://mcui.fekit.cn/#route=plugins/js/inview/)

## Install

```
npm i @fekit/inview
```

or

```
yarn add @fekit/inview
```

## Options

```
{
  el      {String}    // Selector
  theme   {String}    // Specify animation theme
  offset  {Number}    // 0-1, The offset when the element triggers the animation
  once    {Number}    // whether to play animation only once
  on:{
    view  {Function}  // Fired when entering the viewport
    none  {Function}  // Fired when leaving the viewport
  }
}
```

## Example

### React

```tsx
import React, { useLayoutEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';

// import @fekit/inview
import Inview from '@fekit/inview';
// import a theme
import Inview from '@fekit/inview/theme/inview@aa.scss';

function Root() {
  const inview: any = useRef(null);
  const { pathname = '' } = useLocation();

  useLayoutEffect(() => {
    // Create an instance
    demo.current = new Inview({
      el: '.am-inview',
      // Specify an animation theme
      theme: 'aa',
    });

    return () => {
      // Destroy instance
      if (demo.current) {
        demo.current.destroy();
      }
    };
  }, []);

  useLayoutEffect(() => {
    // Usually the instance is refreshed when the URL changes, but it can also be refreshed whenever needed
    if (demo.current) {
      demo.current.refresh();
    }
  }, [pathname]);

  return (
    <div>
      <ul>
        <li class="am-inview">the element to be animated</li>
        <li class="am-inview">the element to be animated</li>
      </ul>
    </div>
  );
}
```

### Plain JS

```javascript
import Inview from '@fekit/inview';

const myDemo = new Inview({
  el: '.demo',
  // Specify an animation theme
  theme: 'ab',
  on: {
    view(dom) {
      console.log(dom, 'The element has entered the viewable area');
    },
    none(dom) {
      console.log(dom, 'The element has left the viewable area');
    },
  },
});

window.onload = function() {
  myDemo.refresh();
};
```

## Themes

The following animated themes are currently available:

scss

- @fekit/inview/theme/inview@aa.scss
- @fekit/inview/theme/inview@ab.scss
- @fekit/inview/theme/inview@ac.scss
- @fekit/inview/theme/inview@ad.scss
- @fekit/inview/theme/inview@ae.scss

css

- @fekit/inview/theme/inview@aa.scs
- @fekit/inview/theme/inview@ab.scs
- @fekit/inview/theme/inview@ac.scs
- @fekit/inview/theme/inview@ad.scs
- @fekit/inview/theme/inview@ae.scs

## Version

```
v0.1.2 [Latest version]
1. Updated documentation
2. The original scss theme file also packs a css file
```

```
v0.1.1
Updated documentation
```

```
v0.1.0
Refactored with typescript, the package turned out to be @fekit/mc-inview.
```
