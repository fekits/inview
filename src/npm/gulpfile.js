let gulp = require('gulp');
let clean = require('gulp-clean');
let sass = require('gulp-sass')(require('sass'));
let autoprefixer = require('gulp-autoprefixer');
let mincss = require('gulp-clean-css');
let gcmq = require('gulp-group-css-media-queries');

let cleanup = () => {
  return gulp.src([
    './theme/**/*.css'
  ]).pipe(clean());
};

let style = () => {
  return gulp.src([
    './theme/inview@aa.scss',
    './theme/inview@ab.scss',
    './theme/inview@ac.scss',
    './theme/inview@ad.scss',
    './theme/inview@ae.scss',
  ])
    .pipe(sass())
    .pipe(autoprefixer({
      overrideBrowserslist: [
        '> 1%',
        'last 2 versions',
        'not ie <= 10',
        'ios >= 8',
        'android >= 4.0'
      ],
      cascade: false
    }))
    .pipe(gcmq())
    .pipe(mincss())
    .pipe(gulp.dest('./theme'));

  // .pipe(gulp.dest((file) => {
  //   return file.base;
  // }));
};

// function watch() {
//   gulp.watch('./**/*.scss', style);
// }

// let dev = gulp.series(style, gulp.parallel(watch));
let pro = gulp.series(cleanup, style);

gulp.task('default', pro);
// gulp.task('build', pro);
