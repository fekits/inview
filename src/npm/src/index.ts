// 兼容处理
const requestAnimFrame = (function() {
  const win: any = window;
  return (
    win.requestAnimationFrame ||
    win.webkitRequestAnimationFrame ||
    win.mozRequestAnimationFrame ||
    function(callback = () => {}) {
      setTimeout(callback, 1000 / 60);
    }
  );
})();

class InView {
  param: any;
  once: any;
  tacking: boolean | number = 1;
  tacked: any;
  size: any;
  doms: any;
  theme: any;
  offset: number = 1;
  on: any;
  listener: () => void;
  area: any;
  constructor(param: any) {
    this.param = param || {};
    this.area = param.area || document;
    this.listener = () => {
      requestAnimFrame(() => {
        if (this.once) {
          if (this.tacking && this.tacked < this.size) {
            this.tacking = 0;
            this.__core();
          }
        } else {
          if (this.tacking) {
            this.tacking = 0;
            this.__core();
          }
        }
      });
    };

    this.area.addEventListener('scroll', this.listener, false);
    this.area.addEventListener('resize', this.listener, false);
  }

  // 刷新
  refresh(param: any) {
    this.param = Object.assign(this.param, param);
    const { el, once, theme, on } = this.param;
    this.doms = document.querySelectorAll(el || '.mc-inview') || [];
    this.size = this.doms.length;
    this.once = once;
    this.theme = theme || 'aa';
    this.on = Object.assign({ view: () => {}, none: () => {} }, on || {});

    // 添加标识
    this.doms.forEach((dom: any) => {
      if (!dom.getAttribute('data-theme')) {
        dom.setAttribute('data-theme', this.theme);
      }
      dom.setAttribute('data-inview', 'none');
      dom._inviewStatus_ = 0;
    });

    // 任务排队
    this.tacking = 1;
    this.tacked = 0;

    // 核心代码
    this.__core();
  }

  // 销毁
  destroy() {
    this.doms.forEach((dom: any) => {
      dom.removeAttribute('data-theme');
      dom.removeAttribute('data-inview');
      delete dom._inviewStatus_;
    });
    this.area.removeEventListener('scroll', this.listener, false);
    this.area.removeEventListener('resize', this.listener, false);
  }

  // 核心
  __core() {
    if (this.once) {
      this.tacked = 0;
    }
    this.offset = window.innerHeight * (this.param?.offset || 1);
    for (let i = 0; i < this.size; i++) {
      let dom = this.doms[i];
      if (this.once) {
        this.tacked = this.tacked + (dom._inviewStatus_ ? 1 : 0);
      }
      (dom => {
        let domRect = dom.getBoundingClientRect();
        if (domRect.top < this.offset) {
          if (!dom._inviewStatus_) {
            // const duration = dom.getAttribute('data-duration');
            // const delay = dom.getAttribute('data-delay');
            // if (duration) {
            //   dom.style.transitionDuration = duration;
            // }
            // if (delay) {
            //   dom.style.transitionDelay = delay;
            // }

            dom.setAttribute('data-inview', 'view');

            this.on.view(dom);
            dom._inviewStatus_ = 1;
          }
        } else {
          if (!this.once) {
            if (dom._inviewStatus_) {
              // dom.style.transitionDuration = '0s';
              // dom.style.transitionDelay = '0s';

              dom.setAttribute('data-inview', 'none');
              this.on.none(dom);
              dom._inviewStatus_ = 0;
            }
          }
        }
      })(dom);
    }
    this.tacking = 1;
  }
}

export default InView;
