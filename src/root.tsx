import React from 'react';
import { connect } from 'react-redux';
import { HashRouter, Routes, Route } from 'react-router-dom';
import { LayerView } from '@fekit/react-layer';

import Home from './components/Home';
import Demo from './components/Demo';

const store = ({ home }: any) => ({ home });
const event = (dispatch: any) => ({});
function Root() {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/demo" element={<Demo />}></Route>
      </Routes>
      <LayerView id="root" />
    </HashRouter>
  );
}

export default connect(store, event)(Root);
